DROP DATABASE IF EXISTS examen1802;
CREATE DATABASE examen1802;
use examen1802;


CREATE TABLE puestos(
    id int auto_increment PRIMARY KEY,
    nombre VARCHAR(100)
);

CREATE TABLE jugadores(
    id int auto_increment PRIMARY KEY,
    nombre VARCHAR(100),
    nacimiento DATETIME,
    id_puesto int,
    index(id_puesto),
    foreign key (id_puesto) REFERENCES puestos(id)
);

INSERT INTO puestos(nombre) VALUES
('Portero'),('Defensa'),('Centrocampista'),('Delantero');

INSERT INTO jugadores(nombre, id_puesto, nacimiento)
VALUES
('De Gea', 1,  '1998-05-22'),
('Kepa', 1,  '1997-03-12'),
('Azpilicueta', 2,  '1995-7-08'),
('Sergio Ramos', 2,  '1998-05-22'),
('Jordi Alba', 2,  '1997-03-12'),
('Iñigo Martínez', 2,  '1995-7-08'),
('S. Roberto', 3,  '1997-03-12'),
('Isco', 3,  '1998-05-22'),
('Asensio', 3,  '1997-03-12'),
('Saúl', 3,  '1988-05-22'),
('Busquets', 3,  '1990-03-12'),
('Diego Costa', 4,  '1995-7-08'),
('Rodrigo', 4,  '1998-01-22'),
('Iago Aspas', 4,  '1995-7-08'),
('Suso', 4,  '1994-03-12'),
('Morata', 4,  '1989-02-19');
