<?php
namespace App\Models;

use PDO;
use \Core\Model;

require_once "../core/Model.php";
/**
*
*/
class Puesto extends Model{

    function __construct(){    }

    public static function all(){
        $db = Puesto::db();

        $statement = $db->query('SELECT * FROM puestos');
        $types = $statement->fetchAll(PDO::FETCH_CLASS,Puesto::class);
        return $types;
    }

    public function jugadores(){
        $db = Jugador::db();
        $statement = $db->prepare('SELECT * FROM jugadores WHERE type_id = :id');
        $statement->bindValue(':id', $this->id);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS, Jugador::class);

        return $jugadores;
    }

    public static function paginate($size = 10){
        if(isset($_REQUEST["page"])){
            $page = (integer) $_REQUEST["page"];
        }else{
            $page = 1;
        }

        $offset = ($page - 1) * $size;

        $db = Jugador::db();

        $statement = $db->prepare('SELECT * FROM puestos LIMIT :pagesize OFFSET :offset');
        $statement->bindValue(":pagesize", $size, PDO::PARAM_INT);
        $statement->bindValue(":offset", $offset, PDO::PARAM_INT);
        $statement->execute();
        $jugadores = $statement->fetchAll(PDO::FETCH_CLASS,Jugador::class);
        return $jugadores;
    }

    public static function rowCount(){
        $db = Puesto::db();

        $statement = $db->prepare('SELECT count(id) as count FROM puestos');
        $statement->execute();
        $rowCount = $statement->fetch(PDO::FETCH_ASSOC);
        return $rowCount["count"];
    }

    public static function find($id){
        $db = Puesto::db();

        $statement = $db->prepare("SELECT * FROM puestos WHERE id=:id");
        $statement->execute(array(":id" => $id));
        $statement->setFetchMode(PDO::FETCH_CLASS, Puesto::class);
        $puesto = $statement->fetch(PDO::FETCH_CLASS);
        return $puesto;
    }

    public static function findByType($idType){
        $db = Jugador::db();
        $statement = $db->prepare("SELECT * FROM puestos WHERE id=:id");
        $statement->execute(array(":id" => $idType));
        $statement->setFetchMode(PDO::FETCH_CLASS, Jugador::class);
        $puesto = $statement->fetch(PDO::FETCH_CLASS);
        return $puesto->nombre;
    }

    public function delete(){
        $db = Jugador::db();
        $statement = $db->prepare("DELETE FROM jugadores WHERE id LIKE :id");
        $statement->bindValue(":id", $this->id);
        return $statement->execute();
    }

    public function __get($atributoDesconocido){
        if (method_exists($this, $atributoDesconocido)) {
            $this->$atributoDesconocido = $this->$atributoDesconocido();
            return $this->$atributoDesconocido;
            // echo "<hr> atributo $x <hr>";
        } else {
            return "";
        }
    }
}
