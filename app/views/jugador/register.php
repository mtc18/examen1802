<!DOCTYPE html>
<html>
<?php require "../app/views/parts/head.php" ?>
<head>
    <title>Nuevo Jugador</title>
</head>
<body>
    <?php require "../app/views/parts/header.php" ?>
    <main role="main" class="container">
      <br>
      <div class="starter-template">
        <form action="/jugador/registerJugador" method="post">
            <div class="form-group">
                <label for="nombre">Nombre:</label>
                <input type="text" class="form-control" name="nombre">
            </div>
            <div class="form-group">
                <label for="nacimiento">Fecha de Nacimiento:</label>
                <input type="text" class="form-control" name="nacimiento">
            </div>
            <div class="form-group">
                <label for="tipo">Tipo de Puesto:</label>
                <select name="tipo" class="form-control">
                    <?php foreach ($types as $puesto): ?>
                        <option value="<?php echo $puesto->id ?>"><?php echo $puesto->nombre ?></option>
                    <?php endforeach ?>
                </select>
            </div>
            <button type="submit" class="btn btn-default">Registrar</button>
        </form>
    </div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
