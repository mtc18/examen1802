<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>

  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1>Lista de Jugadores</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombre</th>
            <th>Fecha de nacimiento</th>
            <th>Puesto</th>
            <th>Operaciones</th>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($jugadores as $j): ?>
            <tr>
              <td><?php echo $j->id ?></td>
              <td><?php echo $j->nombre ?></td>
              <td>
                <?php echo $j->nacimiento->format('d-m-Y') ?>
              </td>
              <td><?php echo $j->puesto->nombre ?></td>
<!--               <td><?php echo $j->id_puesto  ?></td>
-->              <td>
  <!--<a class="btn btn-success" href="/order/add/<?php echo $j->id ?>">Añadir al carrito</a>-->

  <a class="btn btn-primary" href="/jugador/delete/<?php echo $j->id ?>">Borrar</a>
</td>
</tr>
<?php endforeach ?>
</tbody>
</table>
<hr>
Paginas:
<?php for ($i = 1;$i <= $pages;$i++){ ?>
<?php if ($i != $page): ?>
  <a href="/jugador/index?page=<?php echo $i ?>" class="btn">
    <?php echo $i ?>
  </a>
<?php else: ?>
  <span class="btn">
    <?php echo $i ?>
  </span>
<?php endif ?>
<?php } ?>
<hr>
<a href="/jugador/register">Nuevo Jugador</a>
</div>
</main>
<?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
