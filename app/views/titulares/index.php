<!doctype html>
<html lang="es">
<?php require "../app/views/parts/head.php" ?>
<body>
  <?php require "../app/views/parts/header.php" ?>
  <main role="main" class="container">
    <br>
    <div class="starter-template">
      <h1>Titulares:</h1>
      <table class="table table-striped">
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Puesto</th>
            <th>Fecha de Nacimiento</th>
          </tr>
        </thead>
        <tbody>
          <th>.</th>
          <th>.</th>
          <th>.</th>
          <th>.</th>
        </tbody>
      </table>
    </div>
  </main>
  <?php require "../app/views/parts/footer.php" ?>
</body>
<?php require "../app/views/parts/scripts.php" ?>
</html>
