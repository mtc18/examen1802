<?php
namespace App\Controllers;


use \App\Models\Jugador;
use \App\Models\Puesto;
use \App\Models\Basket;
//use \App\Models\Titular;

class TitularesController{

    var $basket;
    function __construct(){    }

    public function index(){
        if(isset($_SESSION["basket"])){
            $basket = $_SESSION["basket"];
        }else{
            $basket = [];
        }
        $numero = 5;
        $rowCount = count($basket);

        $pages = ceil($rowCount / $numero);
        isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;

        require "../app/views/titulares/index.php";
    }

    public function remove($args){
        $id = (int)$args[0];
        $jugador = Jugador::find($id);
        $basket = $_SESSION["basket"];
        unset($basket[$id]);

        $_SESSION["basket"] = $basket;

        $this->index();
    }

    public function add($args){
        if(isset($_SESSION["basket"])){
            $basket = $_SESSION["basket"];
        }else{
            $basket = [];
        }
        $id = (int)$args[0];
        if(array_key_exists($id,$basket)){
            $jugador = $basket[$id];

            $jugador->quantity = $jugador->quantity + 1;
        }else{
            $jugador = Product::find($id);

            $jugador->quantity = 1;
        }
        $basket[$id] = $jugador;
        $_SESSION["basket"] = $basket;

        header("Location:/jugador");
    }


    public function store(){
        if(isset($_SESSION["basket"]) && !empty($_SESSION["basket"])){
            $basket = $_SESSION['basket'];
            $order = new Order();
            $order->date = date('Y/m/d');
            $user = $_SESSION["user"];
            $order->user_id = $user->id;
            $priceOrder = 0;
            foreach ($basket as $product) {
                $priceOrder = $priceOrder + $product->price * $product->quantity;
            }
            $order->price = $priceOrder;
            $order->id = $order->insert();

            foreach ($basket as $product) {
                $order->addProduct($product);
                    // $product->order_id = $order->id;
                    // $product->save();
                echo "<hr>";
            }
            unset($_SESSION["basket"]);
            $basket = $_SESSION["basket"];
        }
        header('Location:/order');
            // $this->index();
    }

}
