<?php
namespace App\Controllers;


use \App\Models\Jugador;
use \App\Models\Puesto;

class JugadorController{

    function __construct(){ }

    public function index(){
        $numero = 5;
        $jugadores = Jugador::paginate($numero);
        $rowCount = Jugador::rowCount();

        $pages = ceil($rowCount / $numero);
        isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;
        require "../app/views/jugador/index.php";
    }

    public function delete($args){
        $id = (int)$args[0];
        $jugador = Jugador::find($id);
        $jugador->delete();

        header("Location:/jugador");
    }

    public function register(){
        $types = Puesto::all();
        require "../app/views/jugador/register.php";
    }

    public function registerJugador(){
        if(isset($_REQUEST["nombre"]) && isset($_REQUEST["nacimiento"])
            && isset($_REQUEST["id_puesto"])){
            $jugador = new Jugador();
        $jugador->nombre = $_REQUEST["nombre"];
        $jugador->nacimiento = $_REQUEST["nacimiento"];
        $jugador->id_puesto = $_REQUEST["id_puesto"];
        $jugador->insert();

        header("Location:/jugador/");
    }else{

        require "../app/views/jugador/register.php";
    }
}




}
