<?php
namespace App\Controllers;

use \App\Models\Jugador;
use \App\Models\Puesto;


class PuestoController{

    function __construct(){   }

    public function index(){
        $numero = 5;
        $types = Puesto::paginate($numero);
        $rowCount = Puesto::rowCount();

        $pages = ceil($rowCount / $numero);
        isset($_REQUEST["page"]) ? $page =(int) $_REQUEST["page"] : $page = 1;
    }
    public function delete($args){
        $id = (int)$args[0];
        $puesto = Puesto::find($id);
        $puesto->delete();

        header("Location:/jugador");
    }

}
